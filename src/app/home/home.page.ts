import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  value1: string = "";
  value2: string = "";
  value3: string = "";
  operador: string = "";

  constructor(public alertController: AlertController) {}

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async sumar(){
    
    if(this.value1 != "" && this.value2 == ""){

      this.operador = "+"
    };
  }

  async restar(){
    
    if(this.value1 != "" && this.value2 == ""){

      this.operador = "-"
    };
  }

  async multiplicar(){
    
    if(this.value1 != "" && this.value2 == ""){

      this.operador = "*"
    };
  }

  async dividir(){
    
    if(this.value1 != "" && this.value2 == ""){

      this.operador = "/"
    };
  }

  async resultado(){
    
    switch(this.operador){
      case "*":
        this.value3 = (+this.value1) * (+this.value2)+"";
        break;

      case "/":
        if(this.value2 == "0"){
          this.value3 = "No se puede dividir por 0"
        }else this.value3 = (+this.value1) / (+this.value2)+"";
        break;

      case "-":
        this.value3 = (+this.value1) - (+this.value2)+"";
        break;

      case "+":
        this.value3 = (+this.value1) + (+this.value2)+"";
        break;
    }
    this.operador = "res";


  }

  async add(valor){

    if(this.operador == "res" ){
      this.reset();
      this.add(valor);
      
    }else if(this.operador == ""){
      this.value1 = this.value1+valor;
    }
    else {
      this.value2 = this.value2+valor;
    }
  }

  async reset(){
    this.operador = "";
    this.value1 = "";
    this.value2 = "";
  }


}


 
